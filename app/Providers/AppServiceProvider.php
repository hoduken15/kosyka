<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->register();

        \Gate::define('Usuario', function ($user) {
            if ($user->profile_type == 1) {
                return true;
            }
        });

        \Gate::define('Profesor', function ($user) {
            if ($user->profile_type == 2) {
                return true;
            }
        });

        \Gate::define('Administrador', function ($user) {
            if ($user->profile_type == 3) {
                return true;
            }
        });
    }
}
