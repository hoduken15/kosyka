<?php

namespace App\Http\Controllers;

use App\Noticias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class NoticiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticias::all();

        return view('noticias.index',[
            'noticias' => $noticias,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('noticias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'titulo'    => 'required',
            'extracto'  => 'required',
            'detalle'   => 'required',
            'fecha'     => 'required',
        );

        if ($request->hasFile('imagen')){
            $filenameWithExt = $request->file('imagen')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $fileNameToStore = date('Y-m-d_H-i-s') . '_' . uniqid() . '_noticia' . '.' .$extension;
            request()->imagen->move(public_path('imagenes/noticias/'), $fileNameToStore);
        } else {
            $fileNameToStore = "dummy-image.png";
        }

        $validator = Validator::make([
            'titulo' => $request->titulo,
            'extracto' => $request->extracto,
            'detalle' => $request->detalle,
            'fecha' => $request->fecha,
        ], $rules);

        if ($validator->fails()) {
            return Redirect::to('noticias/create')
                ->withErrors($validator);
        } else {
            $noticia = new Noticias;
            $noticia->titulo    = Input::get('titulo');
            $noticia->extracto  = Input::get('extracto');
            $noticia->detalle   = Input::get('detalle');
            $noticia->fecha     = Input::get('fecha');
            $noticia->imagen    = $fileNameToStore;
            $noticia->save();

            Session::flash('message', '¡Noticia creada exitosamente!');
            return Redirect::to('noticias');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $noticia = Noticias::find($id);
        return view('noticias.show',[
            'noticia' => $noticia,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $noticia = Noticias::find($id);
        return view('noticias.edit',[
            'noticia' => $noticia,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Noticias  $noticias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'titulo'    => 'required',
            'extracto'  => 'required',
            'detalle'   => 'required',
            'fecha'     => 'required',
        );

        $validator = Validator::make([
            'titulo' => $request->titulo,
            'extracto' => $request->extracto,
            'detalle' => $request->detalle,
            'fecha' => $request->fecha,
        ], $rules);

        if ($validator->fails()) {
            return Redirect::to('noticias/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            $noticia            = Noticias::find($id);
            $noticia->titulo    = $request->titulo;
            $noticia->extracto  = $request->extracto;
            $noticia->detalle   = $request->detalle;
            $noticia->fecha     = $request->fecha;
            if ($request->hasFile('imagen')){
                $filenameWithExt = $request->file('imagen')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
                $extension = $request->file('imagen')->getClientOriginalExtension();
                $fileNameToStore = date('Y-m-d_H-i-s') . '_' . uniqid() . '_noticia' . '.' .$extension;
                request()->imagen->move(public_path('imagenes/noticias/'), $fileNameToStore);
                $noticia->imagen = $fileNameToStore;
            }
            $noticia->save();

            Session::flash('message', '¡Noticia actualizada exitosamente!');
            return Redirect::to('noticias');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Noticias  $noticias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $noticia = Noticias::find($id);
        $noticia->delete();

        Session::flash('message', '¡Noticia eliminada exitosamente!');
        return Redirect::to('noticias');
    }
}
