@extends('adminlte::page')

@section('title', 'Mantenedor de Noticias')

@section('content_header')
    <h1>Mantenedor de Noticias</h1>
@stop

@section('content')
<div class="container">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Mantenedor de Noticias</h3>
        </div>
        <a href="{{route('noticias.index')}}" type="button" class="btn btn-secondary btn-lg btn-block">Listar Noticias</a>
        <a href="{{route('noticias.create')}}" type="button" class="btn btn-secondary btn-lg btn-block">Crear Noticia</a>
    </div>
</div>
@endsection
