@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Inicio</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active">Inicio</li>
            </ol>
        </div>
    </div>
@stop

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="card-deck">
            @foreach($noticias as $noticia)
            <div class="card">
                <img class="card-img-top" height="250" width="250" src="imagenes/noticias/{{ $noticia->imagen }}" alt="Imagen de Noticia">
                <div class="card-body">
                    <h5 class="card-title">{{ $noticia->titulo }}</h5>
                    <p class="card-text">{{$noticia->detalle}}</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">{{$noticia->fecha}}</small>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop