@extends('layouts.login-logout')

@section('content')
<div class="container" style="margin-top: 10%;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="container p-2">
                <p class="titulo-login text-center text-primary h1">
                    Kosyka
                </p>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    {{-- @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $key => $error)
                                    <li>{{ $error }} - {{ $key }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif --}}
                    <div class="p-2 text-center">
                        <div class="form-group row">
                            <input id="name" type="text" class="form-control bg-secondary @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus placeholder="Nombre">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <input id="rut" type="text" class="form-control bg-secondary @error('rut') is-invalid @enderror" name="rut" value="{{ old('rut') }}" autocomplete="rut" autofocus placeholder="Rut">

                            @error('rut')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <input id="email" type="email" class="form-control bg-secondary @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" placeholder="Correo">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <input id="password" type="password" class="form-control bg-secondary @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Contraseña">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                                <input id="password-confirm" type="password" class="form-control bg-secondary" name="password_confirmation" autocomplete="new-password" placeholder="Confirmar Contraseña">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrarse') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="text-center">
                    @guest
                        <a class="btn btn-link" href="{{ route('login') }}">¿Ya tienes cuenta? Ingresa Aquí!</a>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
