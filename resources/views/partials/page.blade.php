@extends('partials.master')

@section('css')
    @stack('css')
    @yield('css')
@stop

@section('body')
    <div class="wrapper">

        @include('partials.navbar')
        @include('partials.nav')

        {{-- Content Wrapper --}}
        <div class="content-wrapper">

            {{-- Content Header --}}
            <div class="content-header">
                <div class="container">
                    @yield('content_header')
                </div>
            </div>

            {{-- Main Content --}}
            <div class="content">
                <div class="container">
                    @yield('content')
                </div>
            </div>

        </div>

        {{-- Footer --}}
        @include('partials.footer')

    </div>
@stop

@section('js')
    @stack('js')
    @yield('js')
@stop
