@extends('adminlte::page')

@section('title', $noticia->titulo)

@section('content_header')
    <h1>{{ $noticia->titulo }}</h1>
@stop

@section('content')
@if(isset($message))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $message }} </h4>
	</div>
@endif

<div class="container">
	<div class="container">
		<div class="d-flex row justify-content-center">
			<div class="card">
				<div class="row no-gutters">
					<div class="col-md-4">
						<img src="/imagenes/noticias/{{$noticia->imagen}}" class="card-img" alt="Noticia-{{$noticia->id}}">
					</div>
					<div class="col-md-8">
						<div class="card-body">
							<h5 class="card-title">{{ $noticia->titulo }}</h5>
							<p class="card-text">{{ $noticia->detalle }}</p>
							<p class="card-text"><small class="text-muted">{{ $noticia->fecha }}</small></p>
							<a href="{{ route('noticias.edit', $noticia) }}" type="button" class="btn btn-success">Editar Noticia</a>
							<form method="POST" action="{{ route('noticias.destroy', $noticia) }}">
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								<button type="submit" class="btn btn-danger">Eliminar Noticia</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection