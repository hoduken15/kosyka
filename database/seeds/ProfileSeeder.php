<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'perfil' => 'Usuario',
        ]);
		DB::table('profiles')->insert([
            'perfil' => 'Profesor',
        ]);
		DB::table('profiles')->insert([
            'perfil' => 'Administrador',
        ]);
    }
}
