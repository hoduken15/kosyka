<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Javier López',
            'email' => 'javierlopezrojas2@gmail.com',
            'password' => Hash::make('123123123'),
            'rut' => '19.573.152-7',
            'profile_type' => 3,
        ]);
    }
}
